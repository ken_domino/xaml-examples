﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace App6
{
    public class Class1 : INotifyPropertyChanged
    {
        public Class1()
        {
            _message_of_the_day = _messages[_i];
            Click = new Command((nothing) =>
            {
                DoWhop();
            });
        }

        private String _message_of_the_day;

        public String MessageOfTheDay
        {
            get
            {
                return _message_of_the_day;
            }
            private set
            {
                _message_of_the_day = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private int _i = 0;

        private List<String> _messages = new List<string>()
        {
            "What's happening?",
            "This is cool, huh?",
            "Working for the weekend.",
            "No time like the present.",
            "Time flies like an arrow.",
            "Microsoft and Xamarin XAML is the best thing since sliced bread!"
        };

        public ICommand Click { protected set; get; }

        private void DoWhop()
        {
            _i++;
            if (_i >= _messages.Count)
                _i = 0;
            MessageOfTheDay = _messages[_i];
        }
    }
}
