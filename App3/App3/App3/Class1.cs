﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3
{
    class Class1
    {
        public static Class1 Singleton { get; private set; } = new Class1();

        private Class1()
        {
        }

        private int _current_value = 1;

        public String CurrentValue
        {
            get
            {
                int i = this._current_value++;
                return "The value is " + i.ToString();
            }
        }
    }
}
