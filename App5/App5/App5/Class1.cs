﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using App5.Annotations;

namespace App5
{
    class Class1 : INotifyPropertyChanged
    {
        public Class1()
        {
            // Set up timer to tick of current time.
            _timer = new Timer((o) =>
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(async () =>
                {
                    CurrentDateTime = DateTime.Now.ToString();
                    LastName = "You should never see this because OnPropertyChanged() is not called!";
                });
            }, null, 0, 1000);
        }

        private Timer _timer;
        public String FirstName { get; set; } = "hi";
        public String LastName { get; set; } = "there";
        private String _current_date_time = DateTime.Now.ToString();
        public String CurrentDateTime
        {
            get
            {
                return _current_date_time;
            }
            set
            {
                _current_date_time = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
